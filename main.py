from flask import render_template, Flask, request, session, redirect, flash
from data.utils import Utils
import os
from logs import Logs

app = Flask(__name__)
app.secret_key = os.urandom(16)


@app.route("/home")
def home():
    return render_template("landing.html")


@app.route("/")
def login_page():
    return render_template("login.html")


@app.route("/login", methods=["POST"])
def login():
    email = request.form["email"]
    password = request.form["password"]

    user = Utils.validate_email_password(email, password)
    if user:
        session["user"] = user["first_name"] + " " + user["last_name"]
        session["email"] = user["email"]
        app.logger.info("Inicio de sesión válido. {} {}".format(email, password))
        return redirect("/home")
    else:
        flash("Correo o contraseña incorrectos")
        app.logger.warning("Inicio de sesión inválido. {} {}".format(email, password))
        return redirect("/")


@app.route("/logout")
def logout():
    p = session.pop("email", None)
    session.clear()
    return redirect("/")


@app.before_request
def before():
    ruta = request.path
    if not 'user' in session and ruta != "/" and ruta != "/login" and ruta != "/logout" and not ruta.startswith("/static"):
        flash("Inicia sesión para continuar")
        return redirect("/")


if __name__ == '__main__':
    log = Logs()
    app.logger.addHandler(log.fileHandler)
    app.logger.addHandler(log.streamHandler)
    app.run('0.0.0.0', 5000, debug=True)

