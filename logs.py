import logging


class Logs:

    #https://docs.python.org/3/library/logging.html
    logFormatStr = '[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s'
    logging.basicConfig(format=logFormatStr, filename="summary.log", level=logging.DEBUG)
    formatter = logging.Formatter(logFormatStr, '%Y-%m-%d %H:%M:%S')
    fileHandler = logging.FileHandler("register.log")
    fileHandler.setLevel(logging.DEBUG)
    fileHandler.setFormatter(formatter)
    streamHandler = logging.StreamHandler()
    streamHandler.setLevel(logging.DEBUG)
    streamHandler.setFormatter(formatter)
